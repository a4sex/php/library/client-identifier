<?php

namespace App\Tests;

use A4Sex\ClientIdentifier;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class ClientIdentifierTest extends KernelTestCase
{
    private RequestStack $requestStack;

    private ClientIdentifier $object;

    public function get($id)
    {
        return self::$kernel->getContainer()->get($id);
    }

    protected function setUp(): void
    {
        self::bootKernel();
        $request = new Request();
        $request->server->set('REMOTE_ADDR', '123.123.12.1');
        $request->headers->set('User-Agent', 'test-agent');
        $this->requestStack = $this->get('request_stack');
        $this->requestStack->push($request);
        $this->object = new ClientIdentifier($this->requestStack);
    }

    public function testIp()
    {
        self::assertEquals('123.123.12.1', $this->object->ip());
    }

    public function testIpin()
    {
        self::assertEquals(2071661569, $this->object->ipin());
    }

    public function testIphash()
    {
        self::assertEquals('9b52158905805c23bf086848de45483d', $this->object->iphash());
    }

    public function testAgent()
    {
        self::assertEquals('test-agent', $this->object->agent());
    }



}
